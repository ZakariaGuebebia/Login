//
//  User.swift
//  FacebookLogin
//
//  Created by Mohamed Habib Jaberi on 29/03/2018.
//  Copyright © 2018 Mohamed Habib Jaberi. All rights reserved.
//

import Foundation
class User : NSObject{
  
  var name: String?
  var id: String?
  var age: String?
  var gender: String?
  
  
  init(_ name: String ,_ id: String ,_ age: String ,_ gender: String ) {
    self.name = name
    self.id = id
    self.age = age
    self.gender = gender

  }
   override init() {}
  
}

//
//  FormViewController.swift
//  FacebookLogin
//
//  Created by Mohamed Habib Jaberi on 30/03/2018.
//  Copyright © 2018 Mohamed Habib Jaberi. All rights reserved.
//

import UIKit
import Firebase

class FormViewController: UIViewController {

  @IBOutlet weak var ageField: UITextField!
  @IBOutlet weak var genderField: UITextField!
  @IBOutlet weak var nameField: UITextField!
  
  var ref: DatabaseReference?
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
  @IBAction func next(_ sender: Any) {
   
    //database reference
    ref = Database.database().reference()
    
    if Auth.auth().currentUser != nil {
      let user = SessionManager.shared.currentUser
      let fireUser =  Auth.auth().currentUser
      
      //preparing the fields
      user?.id = fireUser?.uid
      user?.name = nameField.text!
      user?.age = ageField.text!
      user?.gender = genderField.text!
      
      
      
      let id = user?.id
      let name = user?.name
      let gender = user?.gender
      let age = user?.age
      
      //writting fields to Firebase
    ref?.child("users").child(id!).setValue(["name": name])
    ref?.child("users/\(id!)/age").setValue(age)
    ref?.child("users/\(id!)/gender").setValue(gender)
      
      let vc = self.storyboard?.instantiateViewController(withIdentifier: "fieldsPage") as! FieldsViewController
      self.navigationController?.pushViewController(vc, animated: true)
    }
  
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

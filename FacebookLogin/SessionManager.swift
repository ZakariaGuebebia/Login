//
//  SessionManager.swift
//  FacebookLogin
//
//  Created by Mohamed Habib Jaberi on 29/03/2018.
//  Copyright © 2018 Mohamed Habib Jaberi. All rights reserved.
//


import Foundation

 class SessionManager {
 
  static let shared = SessionManager()
  var currentUser : User?
  
  init() {
    currentUser = User()
  }
}

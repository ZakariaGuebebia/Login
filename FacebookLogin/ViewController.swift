//
//  ViewController.swift
//  FacebookLogin
//
//  Created by Mohamed Habib Jaberi on 26/03/2018.
//  Copyright © 2018 Mohamed Habib Jaberi. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON

class ViewController: UIViewController , FBSDKLoginButtonDelegate {
  
  @IBOutlet weak var emailField: UITextField!
  @IBOutlet weak var pwdField: UITextField!

  @IBAction func emailLogin(_ sender: Any) {
    
    Auth.auth().signIn(withEmail: emailField.text!, password: pwdField.text!) { (user, error) in
      if error != nil {
        print("email login error", error as Any)
      }
      else {
        print("user signed in via email")
        self.isLoggedIn = true
//        self.emailField.text = ""
//        self.pwdField.text = ""
      
        
         
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "fieldsPage") as! FieldsViewController
        self.navigationController?.pushViewController(vc, animated: true)
      }
    }
   
    
  }
  @IBAction func register(_ sender: Any) {
    
    Auth.auth().createUser(withEmail: emailField.text!, password: pwdField.text!) { (user, error) in
      if error != nil {
        print("email login error", error as Any)
      }
      else {
        print("user created via email")
        self.isLoggedIn = true
        self.emailField.text = ""
        self.pwdField.text = ""
       
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FormView") as! FormViewController
        self.navigationController?.pushViewController(vc, animated: true)

      }
    }
  }
  //Firebase reference
  var ref: DatabaseReference?
  
  //create a shared user
  let user = SessionManager.shared.currentUser
  var isLoggedIn = false
  
  @IBOutlet var superView: UIView!

  override func viewDidLoad() {
    super.viewDidLoad()
    view.addSubview(loginButton as UIView)
    loginButton.center = self.view.center
    loginButton.delegate = self
    loginButton.readPermissions = ["email","public_profile"]
    
    //database reference
    ref = Database.database().reference()
    
 
  }
  
  let loginButton = FBSDKLoginButton()
  
  func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
    if error != nil {
      print(error)
    }
    else {
      print("seccesfully logged in with facebook")
      isLoggedIn = true
            FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, first_name, last_name,gender, email, age_range"]).start(completionHandler: { (connection, result, error) in
        if error != nil {
          print(error!)
          return
        }
        else {
          //Json Parsing of result
          let json = JSON(result as Any)
          print(json)
          
        
          //Facebook Access Token
          let accessToken = FBSDKAccessToken.current()
          guard let accessTokenString = accessToken?.tokenString else { return }
          print(accessTokenString)
        
          //Get credentials from FB
          let credential = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
         
          //Sign in using those credentials
          Auth.auth().signIn(with: credential, completion: { (user, err) in
            if err != nil {
              print("sth went wrong!", err!)
            }
            else {
              print("success you'r in firebase")
            
              //Firebase user
              let fireUser =  Auth.auth().currentUser
              
              //preparing the fields
              
              self.user?.id = fireUser?.uid
              self.user?.name = json["name"].stringValue
              self.user?.age = json["age_range"]["min"].stringValue
              self.user?.gender = json["gender"].stringValue
              
              
              
              let id = self.user?.id
              let name = self.user?.name
              let gender = self.user?.gender
              let age = self.user?.age
              
              //writting fields to Firebase
              self.ref?.child("users").child(id!).setValue(["name": name])
              self.ref?.child("users/\(id!)/age").setValue(age)
              self.ref?.child("users/\(id!)/gender").setValue(gender)
              
              
            }
          })
        }
      })
    }
  }
  func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
    print("you logged out!")
    isLoggedIn = false
  }
  
  

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  



}
//  @IBAction func fbLogin(_ sender: Any) {
//
//    let facebookLogin = FBSDKLoginManager()
//    print("Logging In")
//
//    facebookLogin.logIn(withReadPermissions: ["email"], from: self, handler:{(facebookResult, facebookError) -> Void in
//
//      if facebookError != nil { print("Facebook login failed.Error \(String(describing: facebookError))")
//
//      } else if (facebookResult?.isCancelled)! { print("Facebook login was cancelled.")
//
//      } else { print("You’re in")
//        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
//        print("credential  \(credential)")
//
//      }
//
//    })
//
//
//      }

//
//  FieldsViewController.swift
//  FacebookLogin
//
//  Created by Mohamed Habib Jaberi on 28/03/2018.
//  Copyright © 2018 Mohamed Habib Jaberi. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

class FieldsViewController: UIViewController {

  
  
  
  @IBOutlet weak var welcomeLabel: UILabel!
 
  @IBAction func signOut(_ sender: Any) {
    
    let firebaseAuth = Auth.auth()
    do {
      try firebaseAuth.signOut()
      navigationController?.popToRootViewController(animated: true)
    } catch let signOutError as NSError {
      print ("Error signing out: %@", signOutError)
    }
    
    
   
  }
  override func viewDidLoad() {
        super.viewDidLoad()
    
    
    let uid = Auth.auth().currentUser?.uid
    Database.database().reference().child("users").child(uid!).observeSingleEvent(of: .value) { (snapshot) in
      print(snapshot)
      if let dictionary = snapshot.value as? [String : Any]{
        self.welcomeLabel.text = ("Welcome \(dictionary["name"] as? String ?? " ")")
      }
    }
    Auth.auth().currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
      if let error = error {
        print(error as Any)
        return
      }
      print("Your Token ",idToken!)
      //self.getUserRequest(idToken, uid!)
    }
    
        // Do any additional setup after loading the view.
    
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  func  getUserRequest(_ token: String? ,_ userId: String ) {
    let baseUrl = "https://fir-test-6b11e.firebaseio.com/users"
    let urlString = "\(baseUrl)/\(userId).json?auth=\(token!)"
    fetchFromUrl(urlString)

  }
  
  func fetchFromUrl(_ urlString: String) -> () {
    
    guard let url = URL(string: urlString) else { return }
    
    URLSession.shared.dataTask(with: url) { (data, response, error) in
      if error != nil {
        print(error!.localizedDescription)
      }
      
      guard let data = data else { return }
      
      do {
        
        let userData = try JSONDecoder().decode(UserModel.self, from: data)
        //let jsonUserData = JSON(userData)
        
        
        DispatchQueue.main.async {
          
          let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
          if let json = json {
            print(String(describing: json) + "\n")
            
          }
          // print(userData)
          
        }
        
      } catch let jsonError {
        print(jsonError)
      }
      
      
      }.resume()
    
  }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
